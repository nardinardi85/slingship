﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chronos : MonoBehaviour
{
    public void TimeScaleController(int timeScale)
    {
        Time.timeScale = timeScale;
    }
}
