﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Sorry for this script, it was very late...
public class GyrocopterController : MonoBehaviour
{
    KillingObjectBehaviour KOB;
    GameObject player;

    private void Awake()
    {
        if(transform.position.x >= 0)
        {
            transform.Rotate(0, 180, 0);
        }
        Destroy(gameObject, 16);
        KOB = FindObjectOfType<KillingObjectBehaviour>();
        player = FindObjectOfType<SplineRenderer>().gameObject;
    }

    private void Update()
    {        
        transform.Translate(Vector3.right * 0.02f);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.GetComponent<BoxCollider>() == player.GetComponent<BoxCollider>())
        {
            KOB.Death();
        }
    }
}
