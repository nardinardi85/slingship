﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    [SerializeField]
    Transform cameraTransform;
    void Update()
    {
        //just in case i'll need to change camera
        transform.LookAt(cameraTransform);        
    }
}
