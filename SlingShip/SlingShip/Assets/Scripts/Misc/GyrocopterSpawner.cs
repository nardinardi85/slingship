﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyrocopterSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject gyrocopterPrefab;
    float tick;
    [SerializeField]
    float maxTick = 7;

    private void Update()
    {
        if(tick < maxTick)
        {
            tick += Time.deltaTime;
        }
        else
        {
            Instantiate(gyrocopterPrefab, transform);
            tick = 0;
        }
    }
}
