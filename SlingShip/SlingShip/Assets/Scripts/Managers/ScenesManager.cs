﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScenesManager : MonoBehaviour
{    
    private Camera mainCamera;
    private float loadingScreenMinDuration = 3;
    [SerializeField]
    private GameObject LoadingScreenPanel;
    private Image loadingScreenImage;

    public static ScenesManager instance;

    public static ScenesManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        mainCamera = Camera.main;
        if(LoadingScreenPanel != null)
            loadingScreenImage = LoadingScreenPanel.GetComponentInChildren<Image>();
    }

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(instance.gameObject);
            instance = this;
        }
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(5))
        {
            Time.timeScale = 1;
        }
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(6))
        {
            Time.timeScale = 1;
        }
    }

    public void SceneChange(int index)
    {
        StartCoroutine(CoLoadSceneAsyncAfterSeconds(0, loadingScreenMinDuration, index));
    }

    public void SceneChange(int index, float delay)
    {
        StartCoroutine(CoLoadSceneAsyncAfterSeconds(delay, loadingScreenMinDuration, index));
    }

    private IEnumerator CoLoadSceneAsyncAfterSeconds(float delay, float time, int index)
    {        
        yield return new WaitForSecondsRealtime(delay);

        LoadingScreenPanel.SetActive(true);

        yield return new WaitForSecondsRealtime(time);

        var asyncOperation = SceneManager.LoadSceneAsync(index);

        if (SceneManager.GetActiveScene().buildIndex != 0 && asyncOperation.isDone)
        {
            LoadingScreenPanel.SetActive(false);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}