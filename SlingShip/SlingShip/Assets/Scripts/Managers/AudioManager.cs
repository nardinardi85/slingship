﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    #region Fields

    public AudioClip[] audioClips = null;
    AudioSource audioSource;

    #endregion

    #region Properties
    public static AudioManager instance;

    public static AudioManager Instance
    {
        get
        {
            return instance;
        }
    }
    #endregion

    #region Methods
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(instance.gameObject);
            instance = this;
        }
        DontDestroyOnLoad(gameObject);

        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound(int index)
    {
        audioSource.clip = audioClips[index];
        audioSource.Play();
    }
    public void PlaySound(int index, AudioSource altSource)
    {
        altSource.clip = null;
        altSource.clip = audioClips[index];
        altSource.Play();
    }
    #endregion
}
