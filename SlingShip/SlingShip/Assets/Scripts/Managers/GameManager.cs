﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Fields
    public int confirmPanelIndex;

    #endregion

    #region Properties
    public static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }
    #endregion

    #region Methods
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(instance.gameObject);
            instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }

    //Starting form LERP. Returns a straight line between two points
    public Vector2 ReturnStraightLine(Vector2 a, Vector2 b, float time) 
    {
        return a + (b - a) * time;
    }

    //Returns a spline between three points
    public Vector2 QuadraticCurve(Vector2 a, Vector2 b, Vector2 c, float time) 
    {
        Vector2 p0 = ReturnStraightLine(a, b, time);
        Vector2 p1 = ReturnStraightLine(b, c, time);
        return ReturnStraightLine(p0, p1, time);
    }

    //Returns a spline between four points
    public Vector2 CubicCurve(Vector2 a, Vector2 b, Vector2 c, Vector2 d, float time)
    {
        Vector2 p0 = QuadraticCurve(a, b, c, time);
        Vector2 p1 = QuadraticCurve(b, c, d, time);
        return ReturnStraightLine(p0, p1, time);
    }
    #endregion

    public void Indexer(int index)
    {
        confirmPanelIndex = index;
    }
}
