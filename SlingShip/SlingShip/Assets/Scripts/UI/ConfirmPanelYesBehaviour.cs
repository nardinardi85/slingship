﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ConfirmPanelYesBehaviour : MonoBehaviour, ISubmitHandler, ISelectHandler
{
    #region Fields   
    [HideInInspector]
    public int index;

    #endregion

    #region Methods

    public void MoveToCorrectScene()
    {
        ScenesManager.instance.SceneChange(index);        
    }

    public void OnSubmit(BaseEventData eventData)
    {
        MoveToCorrectScene();
    }

    public void OnSelect(BaseEventData eventData)
    {
        index = GameManager.instance.confirmPanelIndex;
    }
    #endregion

}
