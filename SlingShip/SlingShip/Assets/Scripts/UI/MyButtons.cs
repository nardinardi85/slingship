﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MyButtons : MonoBehaviour, ISelectHandler, IDeselectHandler, ISubmitHandler
{
    #region Fields
    public bool isDefaultButton;
    public bool isSelected;

    [HideInInspector]
    public RectTransform myRectTransform;
    [HideInInspector]
    public Button button;
    [HideInInspector]
    public Image selectionImage;
    private Text buttonTxt;
    [HideInInspector]
    public float selectionImageFill;

    [SerializeField]
    private float fillingLerpValue = 0.2f;

    [SerializeField]
    private int confirmIndex = 0;

    public float resizeValue;

    private Vector3 scale;
    private Vector3 defaultScale;

    [SerializeField]
    private Color color = Color.white;
    [HideInInspector]
    public Color defaultColor = Color.white;
    #endregion

    #region Methods
    public void OnDisable()
    {
        selectionImage.fillAmount = 0;
        selectionImage.color = defaultColor;
        selectionImageFill = 0;
    }

    public void Awake()
    {
        button = GetComponent<Button>();
        selectionImage = GetComponent<Image>();
        buttonTxt = GetComponentInChildren<Text>();
        myRectTransform = GetComponent<RectTransform>();
        selectionImageFill = 0;
        defaultScale = myRectTransform.localScale;
        scale = defaultScale * resizeValue;

        if (isDefaultButton)
        {
            isSelected = true;
        }
    }

    protected void Update()
    {
        if (isSelected)
        {
            button.Select();
            if (buttonTxt != null)
            {
                buttonTxt.color = color;
            }
            if (selectionImage != null && selectionImage.fillAmount < 1)
            {
                selectionImageFill += fillingLerpValue * Time.fixedUnscaledDeltaTime;
                selectionImage.fillAmount = Mathf.Clamp01(selectionImageFill);
            }

            myRectTransform.localScale = scale;
        }
    }

    public void OnSelect(BaseEventData eventData)
    {
        AudioManager.instance.PlaySound(0);
        selectionImageFill += fillingLerpValue * Time.fixedUnscaledDeltaTime;
        selectionImage.fillAmount = Mathf.Clamp01(selectionImageFill);
        myRectTransform.localScale = scale;

        isSelected = true;
    }

    public void OnDeselect(BaseEventData eventData)
    {
        if (buttonTxt != null)
        {
            buttonTxt.color = defaultColor;
        }
        myRectTransform.localScale = defaultScale;
        selectionImage.fillAmount = 0;
        selectionImageFill = 0;
        isSelected = false;
    }

    public void OnSubmit(BaseEventData eventData)
    {
        AudioManager.instance.PlaySound(1);
        GameManager.instance.confirmPanelIndex = confirmIndex;
    }
    #endregion
}