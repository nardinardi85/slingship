﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour
{
    [SerializeField]
    GameObject backgroundPanel;
    [SerializeField]
    GameObject pausePanel;
    [SerializeField]
    GameObject mentor;
    [SerializeField]
    Text mentorText;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !pausePanel.activeSelf)
        {
            mentor.SetActive(true);
            mentorText.text = "Why do ye pause, seawolf?";
            backgroundPanel.SetActive(true);
            pausePanel.SetActive(true);
            Time.timeScale = 0;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && pausePanel.activeSelf)
        {
            mentor.SetActive(false);
            backgroundPanel.SetActive(false);
            pausePanel.SetActive(false);
            Time.timeScale = 1;
        }        
    }
}
