﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    [SerializeField]
    protected GameObject mentor;
    [SerializeField]
    protected GameObject backgroundPanel;
    [SerializeField]
    protected Text mentorText;
    [SerializeField]
    protected GameObject anyKeyText;
    [SerializeField]
    protected string[] mentorDialogues;
    [SerializeField]
    protected float maxTimer = 0.25f;
    [SerializeField]
    GameObject player;

    Text tooltip;

    protected float timer = 0;
    protected int i = 0;

    protected virtual void Awake()
    {
        anyKeyText.SetActive(true);
        tooltip = anyKeyText.GetComponent<Text>();
        Time.timeScale = 0;
        backgroundPanel.SetActive(true);
        mentor.SetActive(true);
        mentorText.text = mentorDialogues[0];

        tooltip.text = "Press " + "SPACE" + " to continue or " + "ESC" + " to skip, seawolf!";
    }

    protected virtual void Update()
    {
        if(i <= mentorDialogues.Length - 1)
        {
            timer += Time.unscaledDeltaTime;
            if (Input.GetKeyDown(KeyCode.Space) && timer > maxTimer)
            {
                timer = 0;
                i++;
                mentorText.text = mentorDialogues[i];
            }
        }
        else
        {
            EndDialogue();            
        }
        if(Input.GetKeyDown(KeyCode.Escape) && tooltip.text != "")
        {
            EndDialogue();
        }
    }

    protected virtual void EndDialogue()
    {                     
        mentor.SetActive(false);
        backgroundPanel.SetActive(false);
        i = 0;
        tooltip.text = "";
        anyKeyText.SetActive(false);
        if(player != null)
        {
            Time.timeScale = 1;
        }
    }
}
