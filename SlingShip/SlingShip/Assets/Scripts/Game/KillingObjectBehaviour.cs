﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillingObjectBehaviour : MonoBehaviour
{
    [SerializeField]
    GameObject youLostPanel;
    [SerializeField]
    GameObject backgroundPanel;
    [SerializeField]
    GameObject mentor;
    [SerializeField]
    Text mentorBaloon;
    [SerializeField]
    BoxCollider playerCollider;
    [SerializeField]
    AudioSource[] audioSources;

    private void OnTriggerEnter(Collider other)
    {
        if (other == playerCollider)
        {
            Death();
            Destroy(playerCollider.gameObject);
        }
    }

    public IEnumerator DeathSFX()
    {
        audioSources[2].loop = false;
        AudioManager.instance.PlaySound(5, audioSources[2]);        
        AudioManager.instance.PlaySound(4, audioSources[0]);
        yield return null;
        this.enabled = false;
        //yield return new WaitForSecondsRealtime(1.6f);
        //AudioManager.instance.PlaySound(3, audioSources[1]);
        //yield return new WaitForSecondsRealtime(2.1f);        
        //AudioManager.instance.PlaySound(4, audioSources[2]);
    }
    
    public void Death()
    {
        Time.timeScale = 0;
        StartCoroutine(DeathSFX());
        backgroundPanel.SetActive(true);
        mentor.SetActive(true);
        youLostPanel.SetActive(true);
        mentorBaloon.text = "Ye died, seawolf! \nDon't let Cthulhu eat yer ship!";
    }
}
