﻿using System.Collections;
using UnityEngine;

[ExecuteInEditMode()]
public class SplineRenderer : MonoBehaviour
{
    [SerializeField]
    private Transform startingPoint;

    [SerializeField]
    private Transform windTransform;

    [SerializeField]
    private float windIntensity = 5f;

    [SerializeField]
    private float launchForceMultiplier = 2.5f;

    [SerializeField]
    private LineRenderer lineRenderer;

    [SerializeField]
    private Transform boat;

    [SerializeField]
    private float boatSpeed = 0.2f;

    [SerializeField]
    [Range(3, 20)]
    private int curvePoints = 10;
    private bool movingBoat;

    private bool dragging = false;

    private Vector3 initMousePosition = Vector3.zero;
    private Vector3 dragDirection = Vector3.zero;

    Vector3 initialForce = Vector3.zero;
    Vector3 windForce = Vector3.zero;

    [SerializeField]
    private bool debugCurve;

    private Camera cam;
    [SerializeField]
    KillingObjectBehaviour KOB;

    private void Start()
    {
        cam = Camera.main;
    }

    private void Update()
    {
        if (movingBoat)
            return;

        if (Input.GetMouseButtonDown(0) && Time.timeScale > 0)
        {
            dragging = true;
            initMousePosition = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cam.transform.position.z));
        }

        if (dragging)
        {
            Vector3 currentMousePosition = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cam.transform.position.z));
            dragDirection = (initMousePosition - currentMousePosition).normalized;

            initialForce = startingPoint.position + (new Vector3(dragDirection.x, 0, dragDirection.y) * (initMousePosition - currentMousePosition).magnitude) * launchForceMultiplier;
            windForce = windTransform.forward * windIntensity;

            Debug.DrawLine(startingPoint.position, initialForce, Color.magenta);

            Debug.DrawLine(windTransform.position, windTransform.position + windTransform.forward * windIntensity, Color.magenta);
            if(lineRenderer != null)
            {
                DrawCurve(lineRenderer, new Vector2(startingPoint.position.x, startingPoint.position.z),
                    new Vector2(windForce.x, windForce.z), new Vector2(initialForce.x + windForce.x, initialForce.z + windForce.z), curvePoints);
            }

            Debug.DrawLine(initMousePosition, currentMousePosition, Color.magenta);

            if (Input.GetMouseButtonDown(1))
            {
                dragging = false;
            }

        }
        
        if (Input.GetMouseButtonUp(0) && dragging)
        {
            dragging = false;
            StartCoroutine(CoMoveBoat(new Vector2(startingPoint.position.x, startingPoint.position.z),
                new Vector2(windForce.x, windForce.z), new Vector2(initialForce.x + windForce.x, initialForce.z + windForce.z)));
        }

    }

    private IEnumerator CoMoveBoat(Vector2 startingForce, Vector2 windForce, Vector2 destination)
    {
        movingBoat = true;
        float time = 0;
        while (time < 1)
        {
            time += Time.deltaTime * boatSpeed;

            Vector2 curve = QuadraticCurve(startingForce, windForce, destination, time);
            boat.transform.position = new Vector3(curve.x, boat.transform.position.y, curve.y) ;

            yield return null;
        }
        KOB.Death();
        movingBoat = false;
    }

    public void DrawCurve(LineRenderer lineRenderer, Vector2 startingForce, Vector2 windForce, Vector2 destination, int points = 10)
    {
        lineRenderer.positionCount = points;

        for (int i = 0; i < points; i++)
        {
            Vector2 curvePoint = QuadraticCurve(startingForce, windForce, destination /*new Vector2(endPoint.position.x, endPoint.position.z)*/, (float)i / points);

            lineRenderer.SetPosition(i, new Vector3(curvePoint.x, 0, curvePoint.y));
        }
    }

    //Starting form LERP. Returns a straight line between two points
    public Vector2 ReturnStraightLine(Vector2 a, Vector2 b, float time)
    {
        return a + (b - a) * time;
    }

    //Returns a spline between three points
    public Vector2 QuadraticCurve(Vector2 a, Vector2 b, Vector2 c, float time) //
    {
        Vector2 p0 = ReturnStraightLine(a, b, time);
        Vector2 p1 = ReturnStraightLine(b, c, time);
        return ReturnStraightLine(p0, p1, time);
    }

    //Returns a spline between four points
    public Vector2 CubicCurve(Vector2 a, Vector2 b, Vector2 c, Vector2 d, float time)
    {
        Vector2 p0 = QuadraticCurve(a, b, c, time);
        Vector2 p1 = QuadraticCurve(b, c, d, time);
        return ReturnStraightLine(p0, p1, time);
    }
}
