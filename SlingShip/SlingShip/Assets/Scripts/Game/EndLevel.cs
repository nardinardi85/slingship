﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndLevel : MonoBehaviour
{
    [SerializeField]
    GameObject youWonPanel;
    [SerializeField]
    GameObject backgroundPanel;
    [SerializeField]
    GameObject mentor;
    [SerializeField]
    Text mentorBaloon;
    [SerializeField]
    BoxCollider playerCollider;
    [SerializeField]
    AudioSource[] audioSources;

    private void OnTriggerEnter(Collider other)
    {
        if(other == playerCollider)
        {
            Time.timeScale = 0;
            backgroundPanel.SetActive(true);
            mentor.SetActive(true);
            youWonPanel.SetActive(true);
            mentorBaloon.text = "Ye made it, seawolf! \nNow move on for the next booty!";
            StartCoroutine(EndLevelSFX());
        }        
    }

    public IEnumerator EndLevelSFX()
    {
        audioSources[2].loop = false;
        AudioManager.instance.PlaySound(6, audioSources[2]);
        yield return new WaitForSecondsRealtime(1f);
        AudioManager.instance.PlaySound(7, audioSources[0]);
        yield return new WaitForSecondsRealtime(1.6f);
        AudioManager.instance.PlaySound(6, audioSources[1]);
        yield return new WaitForSecondsRealtime(2.1f);
        AudioManager.instance.PlaySound(8, audioSources[2]);
    }
}
